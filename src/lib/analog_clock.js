/*
    returns: [ centerx, centery, X coord for end of line, Y coord for end of line ]
    hand: hour, minute, second
    date: new Date() or compatible date object
    radius: radius of the circle in pixels
    centerx: center X coord of the circle
    centery: center Y coord of the circle
    options: { handlength: length in pixels of hand (optional) }
*/

const RADIANS = 57.29577951;

export default function ClockHand(
  hand,
  date,
  radius,
  centerx,
  centery,
  options = {}
) {
  let handlength;
  let angle;
  switch (hand) {
    case "second":
      handlength = options.handlength || radius - 1;
      angle = date.getSeconds() * 6;
      break;
    case "minute":
      handlength = options.handlength || radius - 4;
      angle = date.getMinutes() * 6;
      break;
    case "hour":
      handlength = options.handlength || radius - 13;
      angle = date.getHours() * 30 + Math.round((date.getMinutes() / 12) * 6);
      break;
  }
  return {
    centerx: centerx,
    centery: centery,
    handx: Math.floor(32 + Math.sin(angle / RADIANS) * handlength),
    handy: Math.floor(32 - Math.cos(angle / RADIANS) * handlength),
  };
}
