import mongoose from "mongoose";
import fs from "fs";
import csv from "csv-parser";
import { getCompassDirection, getDistance } from "geolib";

const url = "mongodb://mongodb:27017/geodb";

const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

// GeoPoint Schema
const GeoPoint = new Schema({
  city: String,
  state: String,
  tz: String,
  location: {
    type: {
      type: String,
      enum: ['Point'],
      default: 'Point'
    },
    coordinates: {
      type: [Number],
      default: [0, 0],
      index: { type: '2dsphere', sparse: false },
      required: true
    },
  }
}
);

export default class GeoCoding {
  constructor() {
    this.result = [];
    this.databaseLoaded = true;
    this.cities = [];
    var parent = this;
    this.connectToDatabase()
      .then(() => {
        this.loadDatabase(parent)
      });
  }

  async connectToDatabase() {
    this.db = await mongoose.createConnection(url, {
      useCreateIndex: true,
      useNewUrlParser: true,
      useUnifiedTopology: true
    }).on('error', console.error.bind(console))
      .on('disconnected', this.connectToDatabase)
      .once('open', () => {
        console.log(`Connected to MongoDB`)
      })
      .catch((error) => {
        console.error(`cant connect to mongo: `, error);
      });

    this.geoPointModel = await this.db.model('cities', GeoPoint);
  }

  async loadDatabase(parent) {
    // Do in mongoose
    if (parent.databaseLoaded) return;
    parent.cities = [];
    await parent.geoPointModel.deleteMany({});
    console.log("Loading data");
    fs.createReadStream(__dirname + "/../databases/US_Cities.tsv")
      .pipe(csv({ separator: "\t", headers: false }))
      .on("data", (data) => {
        const object = {
          city: data[1],
          state: data[10],
          tz: data[17],
          location: {
            type: "Point",
            coordinates: [parseFloat(data[5]), parseFloat(data[4])],
          },
        };

        parent.cities.push(object);
      })
      .on("end", async () => {
        process.stdout.write(`Writing out ${parent.cities.length} cities\n`);
        await parent.geoPointModel.insertMany(parent.cities, (err, docs) => {
          if (err) return console.error(err);
          console.log(`Inserted ${docs.length}`);
        });

        console.log(parent.db.hotspot.aggregate([{ $indexStats: {} }]));
        parent.databaseLoaded = true;
      });
  }

  async getNearestCities(lat, lon, callback) {
    if (!this.databaseLoaded) {
      callback.location = [];
      return false;
    }
    if (!lat && !lon) return;

    try {
      this.geoPointModel.find(
        {
          location: {
            $near: {
              $geometry: {
                type: "Point",
                coordinates: [lon, lat],
                $maxDistance: 1000,
              },
            },
          },
        },
        null,
        { limit: 3 }).exec(
          (err, results) => {
            if (err) return console.error(err);
            if (results)
              results.forEach((data, index) => {
                data.distance = getDistance(
                  {
                    latitude: lat,
                    longitude: lon,
                  },
                  {
                    latitude: data.location.coordinates[1],
                    longitude: data.location.coordinates[0],
                  }
                );
                data.compassDirection = getCompassDirection(
                  {
                    latitude: lat,
                    longitude: lon,
                  },
                  {
                    latitude: data.location.coordinates[1],
                    longitude: data.location.coordinates[0],
                  }
                );
                this.result[index] = data;
              });
          });
    } catch (err) {
      console.log(err);
    } finally {
      callback.location = this.result;
    }
  }
}
