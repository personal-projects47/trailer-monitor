import mcpadc from "mcp-spi-adc";

const thirteen = 300;
export default class MCP3008 {
  constructor() {
    const batterySensor = mcpadc.openMcp3008(
      0,
      { busNumber: 0, deviceNumber: 1 },
      (err) => {
        if (err) throw err;
        this.averagearray = [];

        setInterval(() => {
          batterySensor.read((err, reading) => {
            if (err) throw err;

            if (this.averagearray.length > 30) this.averagearray.splice(Math.floor(this.averagearray.length/3),15);
            this.averagearray.push(reading.rawValue);
            this.averagearray.sort();


            this.voltage =
              Math.round(
                13 *
                  (this.averagearray[Math.floor(this.averagearray.length / 2)] /
                    thirteen) *
                  10
              ) / 10;
            this.percentage =
              Math.floor(((this.voltage - 11) / 2.6) * 1000) / 1000;
          });
        }, 1000);
      }
    );
  }
}
