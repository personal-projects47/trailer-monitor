import gpsd from "node-gpsd-client";

import display from "./lib/oled/oled";
import mcp3008 from "./lib/mcp3008";

import { Gpio } from "onoff";

import geospatial from './lib/geospatial';

process.stdin.resume();//so the program will not close instantly

function exitHandler(options, exitCode) {
  console.log(`Exiting!? ${exitCode}`);
  if (options.cleanup) console.log('clean');
  if (exitCode || exitCode === 0) console.log(exitCode);
  if (options.exit) process.exit();
}

//do something when app is closing
process.on('exit', exitHandler.bind(null, { cleanup: true }));

//catches ctrl+c event
process.on('SIGINT', exitHandler.bind(null, { exit: false }));

// catches "kill pid" (for example: nodemon restart)
process.on('SIGUSR1', exitHandler.bind(null, { exit: false }));
process.on('SIGUSR2', exitHandler.bind(null, { exit: false }));

//catches uncaught exceptions
process.on('uncaughtException', exitHandler.bind(null, { exit: false }));

process.on('warning', (warning) => {
  console.warn(warning.name);    // Print the warning name
  console.warn(warning.message); // Print the warning message
  console.warn(warning.stack);   // Print the stack trace
});

const geo = new geospatial();


var mcp = new mcp3008();

var oled = new display();

try {
  const button = new Gpio(26, "in", "rising", { debounceTimeout: 10 });

  button.watch((err) => {
    if (err) throw err;
    oled.currentScreen++;
    oled.refreshDisplay();
  });
} catch (err) {
  console.error(err);
}

const client = new gpsd({
  port: process.env.GPSD_SERVICE_PORT,
  hostname: process.env.GPSD_SERVICE_HOST,
  parse: true,
});

client.on("connected", () => {
  console.log(`GPSD Connected!`);
  client.watch(
    {
      class: "WATCH",
      json: true,
      scaled: true
    }
  )
});

client.on('error', err => {
  console.log(`Gpsd error: ${err.message}`)
});

client.on("TPV", async (tpvData) => {
  await geo.getNearestCities(tpvData.lat, tpvData.lon, oled);
  oled.gpslat = tpvData.lat;
  oled.gpslon = tpvData.lon;
  oled.gpsalt = tpvData.alt;
});

setInterval(() => {
  oled.batterypct = mcp.percentage;
  oled.batteryvolt = mcp.voltage;
}, 10000);

client.connect();
