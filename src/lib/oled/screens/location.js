import FontPack from "oled-font-pack";
import { table, getBorderCharacters } from "table";

let townscrolls = {};

export default (father) => {
  father.oled.setCursor(0, 32);
  father.oled.writeString(
    FontPack['small_6x8'],
    1,
    `${Math.round(father.gpslat * 10000) / 10000},${Math.round(father.gpslon * 10000) / 10000
    }`,
    1,
    false
  );
  father.oled.setCursor(0, 42);
  father.oled.writeString(
    FontPack['small_6x8'],
    1,
    `Alt : ${Math.ceil(father.gpsalt * 3.28084)}'`,
    1,
    false
  );
  father.oled.setCursor(0, 54);
  father.oled.writeString(
    FontPack['small_6x8'],
    1,
    `${father.date.toLocaleDateString()} ${father.date.toLocaleTimeString(
      "en-US",
      {
        hour: "2-digit",
        minute: "2-digit",
        ampm: true,
      }
    )}`
  );

  if (father.location) {
    let starty = 0;
    let config = {
      columns: {
        0: {
          alignment: "left",
          width: 21,
        },
        1: {
          alignment: "right",
          width: 7,
        },
      },
      border: getBorderCharacters(`void`),
      singleLine: true,
      columnDefault: {
        paddingLeft: 0,
        paddingRight: 1,
      }
    };
    father.location.forEach((location) => {
      father.oled.setCursor(0, starty);

      let citystate = `${location.city}, ${location.state}`.trim();

      if (citystate.length > 20) {
        if (!townscrolls[citystate]) {
          townscrolls[citystate] = {};
          townscrolls[citystate].start = 0;
          townscrolls[citystate].end = 20;
          townscrolls[citystate].direction = "left";
        } else {
          console.log(townscrolls[citystate]);
          if (townscrolls[citystate].direction === "left") {
            townscrolls[citystate].start++;
            townscrolls[citystate].end++;
          } else {
            townscrolls[citystate].start--;
            townscrolls[citystate].end--;
          }
          if (
            townscrolls[citystate].direction === "left" &&
            townscrolls[citystate].end >= citystate.length
          ) {
            townscrolls[citystate].direction = "right";
          } else if (
            townscrolls[citystate].direction === "right" &&
            townscrolls[citystate].start <= 0
          ) {
            townscrolls[citystate].direction = "left";
          }
        }
        citystate = citystate.substring(
          townscrolls[citystate].start,
          townscrolls[citystate].end
        );
      }
      let distance = `${Math.round(location.distance * 0.00062137)}m ${location.compassDirection
        }`;
      father.oled.writeString(
        FontPack['tiny_4x6'],
        1,
        table([[citystate, distance.trim()]], config)
      );
      starty += 7;
    });
  }
};
