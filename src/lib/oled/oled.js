import Oled from "../../oled-spi";
import fs from "fs";

const conf = {
  width: 128,
  height: 64,
  dcPin: 27,
  rstPin: 17,
};

export default class monitorDriver {
  constructor() {
    this.screenFunctions = {};

    const screendir = fs.readdirSync(`${__dirname}/screens`);

    screendir.forEach((library) => {
      var isLibrary =
          library.split(".").length > 0 && library.split(".")[1] === "js",
        libName = library.split(".")[0].toLowerCase();
      if (isLibrary) {
        try {
          console.log(`loading ${library}`);
          this.screenFunctions[
            libName
          ] = require(`${__dirname}/screens/${libName}`);
        } catch (err) {
          console.log(err);
        }
      }
    });

    this.screens = Object.keys(this.screenFunctions);
    this.currentScreen = 0;

    this.batterypct = Math.random();
    this.batteryvolt = 12.5;
    this.gpslon = 128.0;
    this.gpslat = -64;
    this.gpsalt = 1000;
    this.oled = new Oled(conf);
    this.oled.begin(() => {
      setInterval(() => {
        this.refreshDisplay();
      }, 1000);
    });
  }
  async refreshDisplay() {
    if (this.currentScreen >= this.screens.length) this.currentScreen = 0;

    this.date = new Date();
    this.oled.clearDisplay();
    this.oled.setCursor(0,0); // reset for modules that are too dumb
    try {
      this.screenFunctions[this.screens[this.currentScreen]].default(this);
    } catch (err) {
      console.error(err);
    }
    this.oled.update();
  }
  drawCircle(xc, yc, x, y) {
    this.oled.drawPixel([
      [xc + x, yc + y, 1],
      [xc - x, yc + y, 1],
      [xc + x, yc - y, 1],
      [xc - x, yc - y, 1],
      [xc + y, yc + x, 1],
      [xc - y, yc + x, 1],
      [xc + y, yc - x, 1],
      [xc - y, yc - x, 1],
    ]);
  }
  circleBres(xc, yc, r) {
    var x = 0,
      y = r;
    var d = 3 - 2 * r;
    this.drawCircle(xc, yc, x, y);

    while (y >= x) {
      x++;

      if (d > 0) {
        y--;
        d = d + 4 * (x - y) + 10;
      } else {
        d = d + 4 * x + 6;
      }

      this.drawCircle(xc, yc, x, y);
    }
  }
}
