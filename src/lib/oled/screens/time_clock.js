import ClockHand from "../../analog_clock";

import font from "oled-font-5x7";

const RADIANS = 57.29577951;

export default (father) => {
  var xc = 31,
    yc = 31,
    r2 = 31;

  father.circleBres(xc, yc, r2);
  for (var z = 0; z < 360; z = z + 30) {
    var angle = z / RADIANS;
    let x2 = Math.floor(32 + Math.sin(angle) * 30);
    let y2 = Math.floor(32 - Math.cos(angle) * 30);
    let x3 = Math.floor(32 + Math.sin(angle) * (30 - 5));
    let y3 = Math.floor(32 - Math.cos(angle) * (30 - 5));
    father.oled.drawLine(x2, y2, x3, y3, 1);
  }

  ["hour", "minute", "second"].forEach((hand) => {
    let coords = ClockHand(hand, father.date, 29, 32, 32);
    father.oled.drawLine(
      coords.centerx,
      coords.centery,
      coords.handx,
      coords.handy,
      1
    );
  });
  father.oled.setCursor(65, 0);
  father.oled.writeString(
    font,
    2,
    `${father.date.toLocaleDateString("en-US", { weekday: "short" })}`,
    1,
    false
  );
  father.oled.setCursor(66, 18);
  father.oled.writeString(
    font,
    2,
    `${father.date.toLocaleDateString("en-US", {
      month: "numeric",
      day: "numeric",
    })}`,
    1,
    false
  );
};
