import fs from 'fs';
import FontPack from "oled-font-pack";
import readline from 'readline';
import Stream from 'stream';

export default (father) => {
    father.oled.setCursor(0, 0);
    fs.readdir('/var/log/goveebttemplogger', async (err, files) => {
        for (const file of files) {
            const tempReading = await getLastLine(`/var/log/goveebttemplogger/${file}`, 1);
            const tempParts = tempReading.split(/\s+/);
            const fileParts = file.split(/\-/);
            father.oled.writeString(
                FontPack['oled_5x7'], 2, `${parseInt(((parseFloat(tempParts[2]) * 9 / 5) + 32) * 10) / 10}°f`, 1, false
            );
            father.oled.setCursor(0, 32);
            father.oled.writeString(
                FontPack['oled_5x7'], 2, `${parseFloat(tempParts[3])}% `
            )
        }
    })
}

function getLastLine(fileName, minLength) {
    let inStream = fs.createReadStream(fileName);
    let outStream = new Stream;
    return new Promise((resolve, reject) => {
        let rl = readline.createInterface(inStream, outStream);

        let lastLine = '';
        rl.on('line', function (line) {
            if (line.length >= minLength) {
                lastLine = line;
            }
        });

        rl.on('error', reject)

        rl.on('close', function () {
            resolve(lastLine)
        });
    })
}