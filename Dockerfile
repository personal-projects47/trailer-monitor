FROM node:current-alpine

WORKDIR /usr/src

COPY package*.json ./
RUN apk -U add python3 py3-pip make g++ linux-rpi4-dev linux-headers
RUN yarn

RUN apk del make g++ linux-rpi4-dev linux-headers

COPY . .

CMD [ "npm", "run", "start" ]